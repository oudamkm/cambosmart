/* eslint no-console: 0 */
const path = require('path');
const express = require('express');
const engines = require('consolidate');
const bodyParser = require('body-parser');

const isDeveloping = false;
const port = 4200;//isDeveloping ? 3000 : process.env.PORT;
const app = express();

if (isDeveloping) {
    const webpack = require('webpack');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const config = require('./webpack.config.js');
    const compiler = webpack(config);

    app.use(webpackHotMiddleware(compiler));
    app.use(webpackDevMiddleware(compiler, {
        noInfo: true,
        publicPath: config.PUBLIC_DIR + '/js'
    }));
} else {
    app.set('views', __dirname + '/public');
    app.engine('html', engines.mustache);
    app.set('view engine', 'html');
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(express.static(__dirname + '/public'));
    app.get('*.js', function (req, res, next) {
        req.url = req.url + '.gz';
        res.set('Content-Encoding', 'gzip');
        next();
    });
    app.get('*', function response(req, res) {
        console.log(req.query.id);
        //res.sendFile(path.join(__dirname, 'public/index.html'), {title: "Test"});
        res.render('index', {
            url: req.query.id,
            title: req.query.name,
            description: req.query.des,
            image: req.query.img
        })
    });
}

app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {console.log(err);}
});
