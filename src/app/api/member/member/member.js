import axios from 'axios';
import {CONFIG,API_ENDPOINT} from './../../headers';

export function adminBlockMerchantApi(action){
    return axios.put(API_ENDPOINT + "users/blockmerchant", JSON.stringify(action.status), CONFIG)
        .then(function(response){
           return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}
export function getMerchantDetailApi(action){
    return axios.get(API_ENDPOINT + "users/merchants/" + action.merchant_id, CONFIG)
        .then(function(response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}
export function memberListProductApi(action){
    return axios.get(API_ENDPOINT + "users/"+action.userId+"/products" ,CONFIG)
        .then(function(response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}