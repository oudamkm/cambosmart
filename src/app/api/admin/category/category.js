import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../../headers';
import { clearLoginAdmin, clearState } from './../../../localstorages/local_storage';

/**==========================Oudam===================**/
export function insertCategoryApi(action){
    return axios.post(API_ENDPOINT+ 'admin/category/add', JSON.stringify(action.category.category), AUTH_CONFIG(action.category.token))
        .then (function (response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 401) {
                    clearLoginAdmin();
                } else if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}

export function updateCategoryApi(action){
    return axios.put(API_ENDPOINT+ 'admin/category/update', JSON.stringify(action.category.category), AUTH_CONFIG(action.category.token))
        .then (function (response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 401) {
                    clearLoginAdmin();
                } else if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}

export function deleteCategoryApi(action){
    return axios.put(API_ENDPOINT+ `admin/category/${action.category.id}`, {}, AUTH_CONFIG(action.category.token))
        .then (function (response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 401) {
                    clearLoginAdmin();
                } else if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}

export function fetchCategoryApi(action){
    return axios.get(API_ENDPOINT+ `admin/categories/detail/${action.category.id}`, AUTH_CONFIG(action.category.token))
        .then (function (response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 401) {
                    clearState();
                } else if (error.response.status == 500) {
                    location.href = '/server/error';
                } else {
                    location.href = '/server/down';
                }
            } else {
                location.href = '/server/down';
            }
        })
}

export function fetchParentCategoryApi(action){
    return axios.get(API_ENDPOINT+ `admin/categories/parents?page=${action.category.start}&limit=${action.category.limit}`, AUTH_CONFIG(action.category.token))
        .then (function (response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 401) {
                    clearState();
                } else if (error.response.status == 500) {
                    location.href = '/server/error';
                } else {
                    location.href = '/server/down';
                }
            } else {
                location.href = '/server/down';
            }
        })
}

export function fetchChildCategoryApi(action){
    return axios.get(API_ENDPOINT+ `admin/categories/children/${action.category.id}?page=${action.category.start}&limit=${action.category.limit}`, AUTH_CONFIG(action.category.token))
        .then (function (response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 401) {
                    clearState();
                } else if (error.response.status == 500) {
                    location.href = '/server/error';
                } else {
                    location.href = '/server/down';
                }
            } else {
                location.href = '/server/down';
            }
        })
}