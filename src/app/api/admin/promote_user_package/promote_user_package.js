import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../../headers';
import { clearLoginAdmin, loadState } from './../../../localstorages/local_storage';

/* SAVE PROMOTED USER PACKAGE */
export function apiSavePromotedUSerPackage(action){
    return axios.post(API_ENDPOINT+"admin/userpackage", JSON.stringify(action.packaged), AUTH_CONFIG(loadState() === undefined ? '' : loadState().token))
        .then(function (response) {
            return response.data;
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearLoginAdmin();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        });
}

/* LIST PROMOTED USER PACKAGE */
export function apiListPromotedUSerPackage(action){
    return axios.get(API_ENDPOINT+"admin/userpackages/"+action.packaged.page+"/"+action.packaged.limit, AUTH_CONFIG(loadState() === undefined ? '' : loadState().token))
        .then(function (response) {
            return response.data;
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearLoginAdmin();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        });
}

/* LIST ALL PROMOTED USER PACKAGES */
export function apiListAllPromotedUSerPackage(action){
    return axios.get(API_ENDPOINT+"userpackages", AUTH_CONFIG(loadState() === undefined ? '' : loadState().token))
        .then(function (response) {
            return response.data;
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearLoginAdmin();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        });
}

/* GET PROMOTED USER PACKAGE BY ID */
export function apiGetPromotedUSerPackage(action){
    return axios.get(API_ENDPOINT+"admin/userpackage/"+action.id, AUTH_CONFIG(loadState() === undefined ? '' : loadState().token))
        .then(function (response) {
            return response.data;
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearLoginAdmin();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        });
}

/* UPDATE PROMOTED USER PACKAGE */
export function apiUpdatePromotedUSerPackage(action){
    return axios.put(API_ENDPOINT+"admin/userpackage", JSON.stringify(action.packaged), AUTH_CONFIG(loadState() === undefined ? '' : loadState().token))
        .then(function (response) {
            return response.data;
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearLoginAdmin();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        });
}

/* DELETE PROMOTED USER PACKAGE */
export function apiDeletePromotedUSerPackage(action){
    return axios.delete(API_ENDPOINT+"admin/userpackage/"+action.id, AUTH_CONFIG(loadState() === undefined ? '' : loadState().token))
        .then(function (response) {
            return response.data;
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearLoginAdmin();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        });
}