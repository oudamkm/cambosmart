import axios from 'axios';
import {CONFIG, AUTH_CONFIG, API_ENDPOINT} from './../headers';
import { clearState } from '../../localstorages/local_storage';

export function fetchStoreApi(action){
    return axios.get(API_ENDPOINT+`member/${action.store.id}/store`, AUTH_CONFIG(action.store.token))
        .then(function(response){
            return response.data
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearState();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        })
}

export function updateStoreMapApi(action){
    return axios.put(API_ENDPOINT+"member/store/updateMap", JSON.stringify(action.store.store), AUTH_CONFIG(action.store.token))
        .then(function(response){
            return response.data
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearState();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        })
}


export function updateStoreApi(action){
    return axios.put(API_ENDPOINT+"member/store/update", JSON.stringify(action.store.store), AUTH_CONFIG(action.store.token))
        .then(function(response){
            return response.data
        }).catch(function(error){
            if(error.response.status){
                if(error.response.status == 401){
                    clearState();
                }else if(error.response.status == 500) {
                    window.location.assign('/server/error')
                }else {
                    window.location.assign('/server/down')
                }
            }else {
                window.location.assign('/server/down')
            }
        })
}

export function getUserWithStoreApi(action){
    return axios.get(API_ENDPOINT+`member/${action.store.username}/products`, CONFIG)
        .then(function(response){
            return response.data
        }).catch(function(error){
            if (error.response.status) {
                if (error.response.status == 500) {
                    window.location.assign('/server/error')
                } else {
                    window.location.assign('/server/down')
                }
            } else {
                window.location.assign('/server/down')
            }
        })
}