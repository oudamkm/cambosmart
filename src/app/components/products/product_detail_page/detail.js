/**
 * Created by chhaichivon on 4/12/2017.
 */
import React from '../../../../../node_modules/react';
import RelatedProducts from './related';
import RecentlyProducts from './recently';
import ProductDetail from './product_detail';
import RuleBuyProduct from './rule_buy_products';
class Detail extends React.Component{
    constructor(){
        super();
        this.state ={
            categoryName:'',
            productId: ''
        }
    }
    componentWillMount(){
        this.setState({productId: this.props.location.query.id})
    }

    handleGetCategoryName = (categoryName) => {
        this.setState({categoryName :  categoryName})
    };

    render(){
        return(
            <div>
                <ProductDetail productId={this.state.productId} handleGetCategoryName={this.handleGetCategoryName} />
                <RuleBuyProduct />
                <RelatedProducts productId={this.state.productId} categoryName={this.state.categoryName} />
                <RecentlyProducts />
            </div>
        )
    }
}
export default Detail;