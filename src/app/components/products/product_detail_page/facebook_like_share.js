import React from '../../../../../node_modules/react';

export default class FacebookLikeShare extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            productId: this.props.productId || '',
            name: this.props.products.productName,
            description: this.props.products.productDescription,
            image: this.props.products.productImage[0]
        }
    }

    render(){
        return(
            <div className="row">
                <div className="col-xs-1 share_button">
                   <div className="fb-share-button"
                        data-href={`https://cambosmart.com/products/detail?id=${this.state.productId}&name=${this.state.name.replace(/\s/g, '-')}&des=${this.state.description.substring(0,49).replace(/\s/g, '-')}&img=${this.state.image}`}
                        data-layout="button">
                   </div>
                </div>
                <div className="col-xs-2 like">
                    <div className="fb-like"
                         data-href={`https://cambosmart.com/products/detail?id=${this.state.productId}&name=${this.state.name.replace(/\s/g, '-')}&des=${this.state.description.substring(0,49).replace(/\s/g, '-')}&img=${this.state.image}`}
                         data-layout="standard"
                         data-action="like"
                         data-size="small"
                         data-show-faces="false"
                    >
                    </div>
                </div>
            </div>
        );
    }
}