import React from '../../../../../node_modules/react';

class FacebookComment extends React.Component {

    componentDidMount() {
       window.fbAsyncInit = function() {
            FB.init({
            appId      : '1307001952740433',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.8'
            });
            FB.AppEvents.logPageView();   
        };

        (function(d, s, id){
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    render(){
        return(
            <div className="fb-comments" 
                data-href={"https://cambosmart.com/products/detail/" + this.props.productId}
                data-width="100%" 
                data-order-by="reverse_time" 
                data-numposts="3">
            </div>
        );
        // data-href={"http://localhost:8080/products/detail/" + productIdUrl} 
    }
}
export default FacebookComment;