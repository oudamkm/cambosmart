import React from '../../../../../node_modules/react';
import { zoom } from 'jquery-zoom';
import { connect } from '../../../../../node_modules/react-redux';
import { bindActionCreators } from 'redux';
import { actionAdminGetProduct } from'../../../actions/admin/product/product';
import { Col } from 'react-bootstrap';
import '../../../../../node_modules/react-star-rating/dist/css/react-star-rating.min.css';
import './style.css';
import StoreInformation from './store_information';
//import Galleries from './galleries';
import Galleries from './galleries';
import RatingProduct from './rating_product';
import FacebookComment from './facebook_comment';
import FacebookLikeShare from './facebook_like_share';
import ProductViewTotal from './product_view_total';

class ProductDetail extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            storeId: "",
            categoryName: "",
            store_pro: []
        };
    }

    componentWillReceiveProps(data) {
        this.setState({
            storeId: Object.values(data.adminGetProduct.products[0]._id)[0],
            categoryName: data.adminGetProduct.products[0].categories.categoryName,
            store_pro: data.adminGetProduct.products[0].store_product
        });
        this.props.handleGetCategoryName(data.adminGetProduct.products[0].categories.categoryName);
    }

    componentDidMount() {
        //let productIdUrl = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        this.props.actionAdminGetProduct(this.props.productId);
    }

    render(){
        const products = this.props.adminGetProduct.products;
        let  productView = '';
        let images = [];
        if(this.state.storeId !== "") window.sessionStorage.setItem("storeIdDetail", this.state.storeId);
        if(products !== undefined){
            this.props.adminGetProduct.products.map((product) => {
                productView = product.views;
                images = product.store_product.productImage;
            });
        }

        return(
            <div className="row">
                <Col xs={12} sm={12} md={12} lg={12} className="product-detail">
                    <h3 style={{textTransform: 'capitalize'}}>{this.state.store_pro.productName}</h3>
                    <div className="img-detail">
                        {images.length === 0 ? null :
                            <Galleries images={images} />
                        }
                        <Col xs={4} sm={4} md={4} className="box-user-info" style={{paddingTop: 10}}>
                            {products === undefined ? null :
                                products.length === 0 ? null :
                                    <StoreInformation productId={this.props.productId} products={products} />
                            }
                        </Col>
                    </div>
                    {/* Facebook Plugin */}
                    <Col xs={12} sm={12} md={12} lg={12} style={{marginTop: 15}}>
                        <div style={{marginLeft: 5}}>
                            {products === undefined ? null :
                                products.length === 0 ? null :
                                    <FacebookLikeShare productId={this.props.productId} products={products[0].store_product} />
                            }
                            <div id="frmRate" style={{marginTop: 10}}>
                                {/*<SubscribeStore storeId={this.state.storeId} />
                                &nbsp;&nbsp; */}
                                <RatingProduct productId={this.props.productId} />
                            </div>
                            <p style={{marginTop: 15, wordWrap: 'break-word', whiteSpace: 'pre-line'}}>{this.state.store_pro.productDescription}</p>
                            <ProductViewTotal total={productView}/>
                            <FacebookComment productId={this.props.productId} />
                        </div>
                    </Col>
                </Col>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        adminGetProduct: state.adminGetProduct
    }
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ 
        actionAdminGetProduct
    }, dispatch)
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);