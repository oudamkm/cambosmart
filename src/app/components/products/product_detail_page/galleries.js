import React from '../../../../../node_modules/react';
import { zoom } from 'jquery-zoom';
import Slider from 'react-slick';

import { Col } from 'react-bootstrap';
import Lightbox from 'react-image-lightbox';

export default class ProductDetail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            galleries: this.props.images,
            img: `/images/products/${this.props.images[0]}`,
            photoIndex: 0,
            isOpen: false
        };
    }
    /*componentWillMount() {
        this.setState({galleries: this.props.images, img: `/images/products/${this.props.images[0]}`})
    }*/

    changeImage(path) {
        let index = this.state.galleries.findIndex((item => item === path));
        this.setState({
            img: "/images/products/" + path,
            photoIndex: index
        });
    }

    renderGalleries = () => {
        const settings = {
            className: 'center',
            infinite: false,
            centerPadding: '60px',
            slidesToShow: 5,
            swipeToSlide: true,
            responsive: [ { breakpoint: 364, settings: { slidesToShow: 2} },{ breakpoint: 515, settings: { slidesToShow: 3 } }]
        };

        return(
            <div>
                <Slider {...settings}>
                    {this.state.galleries.map((image, index) =>
                        <a key={index} onClick={() => {this.changeImage(image)}}>
                            <img width="80" height="80" src={"/images/products/" + image}/>
                        </a>
                    )}
                </Slider>
            </div>
        );
    };
    render(){
        const {
            photoIndex,
            isOpen,
        } = this.state;

        return(
            <div>
                <Col xs={8} sm={8} md={8} className="padding_left_right">
                    <Col xs={12} sm={12} md={12} className="padding_left_right">
                        <span className='zoom'>
                            <a href="javascript:void(0)" onClick={() => this.setState({ isOpen: true })}>
                                <img src={this.state.img} />
                            </a>
                        </span>
                    </Col>
                    <Col xs={12} sm={12} md={12} className="padding_left_right sub-gallery">
                        {this.renderGalleries()}
                    </Col>
                </Col>
                {isOpen &&
                    <Lightbox
                        mainSrc={"/images/products/" + this.state.galleries[photoIndex]}
                        nextSrc={"/images/products/" + this.state.galleries[(photoIndex + 1) % this.state.galleries.length]}
                        prevSrc={"/images/products/" + this.state.galleries[(photoIndex + this.state.galleries.length - 1) % this.state.galleries.length]}

                        onCloseRequest={() => this.setState({ isOpen: false })}
                        onMovePrevRequest={() => this.setState({
                            photoIndex: (photoIndex + this.state.galleries.length - 1) % this.state.galleries.length,
                        })}
                        onMoveNextRequest={() => this.setState({
                            photoIndex: (photoIndex + 1) % this.state.galleries.length,
                        })}
                    />
                }
            </div>
        );
    }
}