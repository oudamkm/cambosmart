import React from 'react';
import FileUpload from '../../file_upload';
import './style.css';
import './prism.css';
import {dataURItoBlob} from './../../../../../../utils/image_file_handling';

export default class Image1 extends React.Component {
    constructor() {
        super();
        this.state = {
            img: ''
        };
        this.handleFileChange = this.handleFileChange.bind(this);
    }

    handleFileChange (data) {
        let message = "";
        let file = null;
        if(data.size == 1){
            message = "File was too large, please try another !!";
            file = null;
        }else if(data.size == 0){
            message = "File was too small, please try another !!";
            file = null;
        }else if(data.size == -1){
            message = "File did not match any type of (jpeg, jpg, png, gif) !!";
            file = null;
        }else {
            this.setState({img: data.img});
            message = "";
            file = dataURItoBlob(data.img);
        }
        this.props.setFile1({file: file, name: data.name.toLowerCase().concat('.jpg'), message: message});
    }

    render() {
        return (
            <div className="row">
                <div encType="multipart/form-data">
                    <div className="avatar-photo product">
                        <FileUpload handleFileChange={this.handleFileChange} />
                        <div className="avatar-edit product">
                            <i className="fa fa-camera">
                            </i>
                        </div>
                        <img src={this.state.img} style={{height:"120px",width:"120px"}}/>
                    </div>
                </div>
            </div>
        );
    }
}