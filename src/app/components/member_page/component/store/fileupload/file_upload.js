import React from 'react';
import ReactDOM from 'react-dom';

export default class FileUpload extends React.Component {
    constructor(props){
        super(props);
        this.handleFile = this.handleFile.bind(this);
    }

    handleFile(e) {
        let reader = new FileReader();
        let file = e.target.files[0];
        let img = document.createElement("img");
        let imageUri = "";
        if (!file){
            this.props.handleFileChange({img: null, size: -1, name: ''});
        } else {
            if (!file.name.match(/.(jpg|jpeg|png)$/i)){
                this.props.handleFileChange({img: null, size: -1, name: ''});
            }else {
                if(file.size > 10*1024*1024){
                    this.props.handleFileChange({img: null, size: 1, name: ''});
                }else if(file.size < 10*1024){
                    this.props.handleFileChange({img: null, size: 0, name: ''});
                }else {
                    reader.onload = function(e) {
                        ReactDOM.findDOMNode(this.refs.in).value = '';
                        img.src = e.target.result;
                        img.onload = function () {
                            let canvas = document.createElement('canvas');
                            let ctx = canvas.getContext('2d');
                            let scale = 1;
                            if(img.width > 1092 && img.height < 200){
                                scale = 1 - (img.width - 1092)/img.width;
                            }else if(img.width > 1092 && img.height > 200){
                                scale = ((1 - (img.width - 1092)/img.width) + (1 - (img.height - 200)/img.height)) / 2;
                            }else if(img.width < 1092 && img.height > 200){
                                scale = 1 - (img.height - 200)/img.height;
                            }else {
                                scale = 1;
                            }
                            canvas.width = img.width * scale;
                            canvas.height = img.height * scale;
                            ctx.drawImage(img, 0, 0, img.width * scale, img.height * scale);
                            imageUri = canvas.toDataURL('image/jpeg', file.size < 1024 ? file.size < 100 ? 0.75 : 0.95 : 0.8);
                        };
                        setTimeout(() => {
                            this.props.handleFileChange({img: imageUri, size: file.size/1024, name: file.name.split('.')[0]});
                        }, 1000);
                    }.bind(this);
                    reader.readAsDataURL(file);
                }
            }
        }
    }

    render() {
        return (
            <input ref="in" type="file" accept="image/*" onChange={this.handleFile} />
        );
    }
}