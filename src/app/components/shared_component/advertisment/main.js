import React from 'react';
import Slider from 'react-slick';
import './../../../../../node_modules/slick-carousel/slick/slick.css';
import './../../../../../node_modules/slick-carousel/slick/slick-theme.css';

class Main extends React.Component{
    render(){
        let settings = {
            dots: true,
            infinite: true,
            speed: 700,
            autoplaySpeed:5000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay:true,
            fade:false
        };
        return(
            <div className="advertise_main_page">
                {this.props.display !== undefined ?
                    this.props.display.advertisers.length > 0 ?
                        <Slider {...settings}>
                            { this.props.display.advertisers.map((advertiser, index) => {
                                return (
                                    <div key={index}>
                                        <img
                                            onClick={() => window.open(advertiser.url !== "" ? advertiser.url : `/images/advertisements/${advertiser.image}`, "_blank")}
                                            alt="main-logo" src={`/images/advertisements/${advertiser.image}`}
                                            width="1122px" height="300px"/>
                                    </div>
                                    )
                                })
                            }
                        </Slider>
                        :
                        <div>
                            <img onClick={() => window.open('/icon/advertisements/default/default.jpg', "_blank")}
                                 alt="900x500" src="/icon/advertisements/default/default.jpg" width="1122px" height="300px"/>
                        </div>
                    :
                    <div>
                        <img onClick={() => window.open('/icon/advertisements/default/default.jpg', "_blank")}
                             alt="900x500" src="/icon/advertisements/default/default.jpg" width="1122px" height="300px"/>
                    </div>
                }
                <br/>
           </div>

        );
    }
}

export  default Main;