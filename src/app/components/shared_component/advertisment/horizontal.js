import React from 'react';
import {Carousel}from 'react-bootstrap';
import './advertisment.css';
import Slider from 'react-slick';
import './../../../../../node_modules/slick-carousel/slick/slick.css';
import './../../../../../node_modules/slick-carousel/slick/slick-theme.css';
class Horizontal extends React.Component{

    constructor(props){
        super(props)
    }

    render(){
        var settings = {
            dots: true,
            infinite: true,
            speed: 700,
            autoplaySpeed:5000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay:true,
            fade:false
        };
        return(
            <div>
                <div className="advertise_sub_page horizontal" style={{ marginLeft:"-10px" }}>
                    <div className="detail-page col-xs-12 col-sm-12 col-md-12 col-lg-12 advertise">
                        { this.props.display != undefined ?
                            this.props.display.advertisers.length > 0 ?
                                <Slider {...settings} className="slide-hori">
                                    { this.props.display.advertisers.map((advertiser, index) => {
                                        return (
                                            <div key={index}>
                                                <img onClick={() => window.open(advertiser.url != "" ? advertiser.url : `/images/advertisements/${advertiser.image}`, "_blank")}
                                                    width="273" height="443" alt="273x443" src={`/images/advertisements/${advertiser.image}`}/>
                                            </div>
                                        )
                                    })}
                                </Slider>
                                :
                                <div className="h-image">
                                    <img onClick={() => window.open('/icon/advertisements/default/default_h.jpg', "_blank")} width="273" height="443" alt="273x443" src="/icon/advertisements/default/default_h.jpg"/>
                                </div>
                            :
                            <div className="h-image">
                                <img onClick={() => window.open('/icon/advertisements/default/default_h.jpg', "_blank")} width="273" height="443" alt="273x443" src="/icon/advertisements/default/default_h.jpg"/>
                            </div>
                        }
                        <br/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Horizontal;