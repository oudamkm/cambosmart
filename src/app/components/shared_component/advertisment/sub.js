import React from 'react';
import Slider from 'react-slick';
import './../../../../../node_modules/slick-carousel/slick/slick.css';
import './../../../../../node_modules/slick-carousel/slick/slick-theme.css';
class Sub extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        var settings = {
            dots: true,
            infinite: true,
            speed: 700,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay:true,
            autoplaySpeed:5000,
            fade:false

        };

        return(
            <div className="advertise_sub_page">
                {this.props.display != undefined ?
                    this.props.display.advertisers.length > 0 ?
                        <Slider {...settings}>
                            { this.props.display.advertisers.map((advertiser, index) => {
                                return (
                                    <div key={index}>
                                        <img
                                            onClick={() => window.open(advertiser.url != "" ? advertiser.url : `/images/advertisements/${advertiser.image}`, "_blank")}
                                            width="838" height="298" alt="900x500"
                                            src={`/images/advertisements/${advertiser.image}`}/>
                                    </div>
                                )
                            })}
                        </Slider>
                        :
                        <div>
                            <img
                                onClick={() => window.open('/icon/advertisements/default/default.jpg', "_blank")}
                                width="838" height="298" alt="900x500"
                                src="/icon/advertisements/default/default.jpg"/>
                        </div>
                    :
                    <div>
                        <img
                            onClick={() => window.open('/icon/advertisements/default/default.jpg', "_blank")}
                            width="838" height="298" alt="900x500"
                            src="/icon/advertisements/default/default.jpg"/>
                    </div>
                }
            </div>
        );
    }
}

export default Sub;