import React from 'react';
import {Carousel}from 'react-bootstrap';
import './advertisment.css';
import Slider from 'react-slick';
import './../../../../../node_modules/slick-carousel/slick/slick.css';
import './../../../../../node_modules/slick-carousel/slick/slick-theme.css';

class Vertical extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
        var settings = {
            dots: true,
            infinite: true,
            speed: 700,
            autoplaySpeed:5000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay:true,
            fade:false
        };
        return(
            <div className="advertise_sub_page v">
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6 adv-v">
                    { this.props.display != undefined ?
                        this.props.display.advertisers.length > 0 ?
                            <Slider {...settings} className="advertise-v">
                                { this.props.display.advertisers.map((advertiser, index) =>{
                                    return(
                                        <div key={index}>
                                            <img onClick={() => window.open(advertiser.url != "" ? advertiser.url : `/images/advertisements/${advertiser.image}`,"_blank")} width="409" height="245" alt="409x245" src={`/images/advertisements/${advertiser.image}`} />
                                        </div>
                                    )
                                })}
                            </Slider>
                            :
                            <div className="v-image">
                                <img onClick={() => window.open('/icon/advertisements/default/default.jpg',"_blank")} width="409" height="245" alt="409x245" src="/icon/advertisements/default/default.jpg"/>
                            </div>
                        :
                        <div className="v-image">
                            <img onClick={() => window.open('/icon/advertisements/default/default.jpg',"_blank")} width="409" height="245" alt="409x245" src="/icon/advertisements/default/default.jpg"/>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default Vertical;
