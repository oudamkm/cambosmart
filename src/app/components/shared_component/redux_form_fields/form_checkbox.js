import React, { PropTypes } from "../../../../../node_modules/react";
import { FormGroup, HelpBlock, Row, Col } from "../../../../../node_modules/react-bootstrap";
import { loadLanguage } from './../../../localstorages/local_storage';
export default class FormCheckbox extends React.Component {
    render(){
        const { input, meta, type, id } = this.props;
        return(
            <FormGroup validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
                <FormGroup>
                    <Row>
                        <Col xs={1} sm={1} md={1} lg={1}>
                            <input {...input} id={id} type={type} value="true" style={{width: '25px', height: '25px',marginTop:'0'}}/>
                        </Col>
                        <Col xs={11} sm={11} md={11} lg={11}>
                            <label>
                                <em style={{fontSize: '18px'}}>
                                    {loadLanguage() == "en" ||  loadLanguage() == undefined ? "Agree Term and condition" : "យល់ព្រម"} &nbsp;
                                    <a href="/help/privacy">{loadLanguage() == "en" || loadLanguage() == undefined ? "Learn more" :"មើលលំអិត"}</a>
                                </em>
                            </label>
                        </Col>
                    </Row>
                </FormGroup>
                <HelpBlock>
                    {meta.touched && meta.error ? meta.error : null }
                </HelpBlock>
            </FormGroup>
        );
    }
}
FormCheckbox.propTypes = {
    meta: PropTypes.object
};