import React from 'react';

export default class NotFound extends React.Component {
    render(){
        return (
            <div>
                <center>
                    <h3>ERROR 404 NOT FOUND !</h3>
                    <hr />
                </center>
            </div>
        )
    }
}