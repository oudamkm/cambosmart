import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {fetchViewWebsiteAction} from '../../../../actions/admin/common';

/*fromDate: moment().format("YYYY-MM-DD"), toDate: moment().add(1, 'days').format("YYYY-MM-DD")*/

class VisitorCounting extends React.Component {
    componentWillMount(){
        this.props.fetchViewWebsiteAction({
            fromDate: '1000-1-1',
            toDate: '1000-1-1'
        })
    }
    render(){
        const visitors = this.props.fetchWebsite.visitors;
        return(
            <div>
                <span className="quick_box_text">
                    <h1><b>
                    { visitors === undefined ? null : visitors.length <= 0 ? 0 : visitors[0].total}
                    </b></h1>
                    <p>Total Visits</p>
                </span>
                <span className="quick_box_icon">
                    <i className="fa fa-eye" />
                </span>
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        fetchWebsite: state.fetchWebsite
    };
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({fetchViewWebsiteAction}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(VisitorCounting)