import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { renderHeadDiv, renderDiv } from './render-form';
import './page_style_component.css';

export default class FormCategoryPage extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick(location){
        if(this.props.advertisements != undefined){
            if(this.props.advertisements.length > 0){
                const advertise  = this.props.advertisements.filter(ads => ads.location == location);
                if(advertise != null){
                    this.props.handleClick({
                        location: location,
                        price: advertise[0].price,
                        description: advertise[0].description
                    });
                }
            }
        }
        window.scrollTo(0,0);
    }

    render(){
        const advertisements = this.props.advertisements;
        return(
            advertisements != undefined ?
                <div>
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("schedule-div", "CT1 ", "1122 x 300px", advertisements, "CT1", () => this.handleClick("CT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lgOffset={4} lg={8} style={{left: '28px'}}>
                            {renderHeadDiv("schedule-div", "CT2 ", "(838 x 298px)", advertisements, "CT2", () => this.handleClick("CT2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL1","283 x 443px ", advertisements, "CL1", () => this.handleClick("CL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL2","283 x 443px ", advertisements, "CL2", () => this.handleClick("CL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CML1","409 x 245px ",  advertisements, "CML1", () => this.handleClick("CML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CMR1","409 x 245px ", advertisements, "CMR1", () => this.handleClick("CMR1"))}
                        </Col>
                    </Row>

                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL3","283 x 443px ",  advertisements, "CL3", () => this.handleClick("CL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL4","283 x 443px ", advertisements, "CL4", () => this.handleClick("CL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CML2","409 x 245px ", advertisements, "CML2", () => this.handleClick("CML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CMR2","409 x 245px ", advertisements, "CMR2", () => this.handleClick("CMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL5","283 x 443px ", advertisements, "CL5", () => this.handleClick("CL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL6","283 x 443px ", advertisements, "CL6", () => this.handleClick("CL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CML3","409 x 245px ",  advertisements, "CML3", () => this.handleClick("CML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CMR3","409 x 245px ", advertisements, "CMR3", () => this.handleClick("CMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL7","283 x 443px ",  advertisements, "CL7", () => this.handleClick("CL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL8","283 x 443px ", advertisements, "CL8", () => this.handleClick("CL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CML4","409 x 245px ", advertisements, "CML4", () => this.handleClick("CML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "CMR4","409 x 245px ", advertisements, "CMR4", () => this.handleClick("CMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL9","283 x 443px ",  advertisements, "CL9", () => this.handleClick("CL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "CL10","283 x 443px ",  advertisements, "CL10", () => this.handleClick("CL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}