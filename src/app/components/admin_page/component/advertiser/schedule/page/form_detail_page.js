import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { renderHeadDiv, renderDiv } from './render-form';
import './page_style_component.css';

export default class FormDetailPage extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick(location){
        if(this.props.advertisements != undefined){
            if(this.props.advertisements.length > 0){
                const advertise  = this.props.advertisements.filter(ads => ads.location == location);
                if(advertise != null){
                    this.props.handleClick({
                        location: location,
                        price: advertise[0].price,
                        description: advertise[0].description
                    });
                }
            }
        }
        window.scrollTo(0,0);
    }

    render(){
        const advertisements = this.props.advertisements;
        return(
            advertisements != undefined ?
                <div>
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("schedule-div", "DT1 ", "1122 x 300px", advertisements, "DT1", () => this.handleClick("DT1"))}
                        </Col>
                    </Row>
                    <br />

                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL1","283 x 443px ", advertisements, "DL1", () => this.handleClick("DL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL2","283 x 443px ", advertisements, "DL2", () => this.handleClick("DL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DML1","409 x 245px ",  advertisements, "DML1", () => this.handleClick("DML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DMR1","409 x 245px ", advertisements, "DMR1", () => this.handleClick("DMR1"))}
                        </Col>
                    </Row>

                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL3","283 x 443px ",  advertisements, "DL3", () => this.handleClick("DL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL4","283 x 443px ", advertisements, "DL4", () => this.handleClick("DL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DML2","409 x 245px ", advertisements, "DML2", () => this.handleClick("DML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DMR2","409 x 245px ", advertisements, "DMR2", () => this.handleClick("DMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL5","283 x 443px ", advertisements, "DL5", () => this.handleClick("DL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL6","283 x 443px ", advertisements, "DL6", () => this.handleClick("DL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DML3","409 x 245px ",  advertisements, "DML3", () => this.handleClick("DML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DMR3","409 x 245px ", advertisements, "DMR3", () => this.handleClick("DMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL7","283 x 443px ",  advertisements, "DL7", () => this.handleClick("DL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL8","283 x 443px ", advertisements, "DL8", () => this.handleClick("DL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DML4","409 x 245px ", advertisements, "DML4", () => this.handleClick("DML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "DMR4","409 x 245px ", advertisements, "DMR4", () => this.handleClick("DMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL9","283 x 443px ",  advertisements, "DL9", () => this.handleClick("DL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "DL10","283 x 443px ",  advertisements, "DL10", () => this.handleClick("DL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
