import React from 'react';
import moment from 'moment';

const getAdvertisement = (advertisements, location) => {
    return advertisements.find(advertisement => advertisement.location == location)
};
export const renderHeadDiv = (className, title, defaultSize, advertisements, location, handleClick) => {
    return(
        getAdvertisement(advertisements, location) != null ?
            getAdvertisement(advertisements, location).advertise.filter(ads => ads.expireDate >= new Date().getTime()).length >= 20 ?
                <a style={{pointerEvents: "none", cursor: "not-allowed"}}>
                    <div style={{ marginLeft: '-15px', background: '#ddd', textAlign: 'center', border: '1px solid #ccc', height: '80px'}}>
                        <h4>{title}(<span style={{fontWeight: 'bold', fontSize: '12px'}}>{defaultSize}</span>)</h4>
                        <span className="schedule-current">Not available</span>
                    </div>
                </a>
                :
                <a onClick={handleClick}>
                    <div className={className}>
                        <h4>{title}(<span style={{fontWeight: 'bold', fontSize: '12px'}}>{defaultSize}</span>)</h4>
                        <span className="schedule-current">C : {getAdvertisement(advertisements, location).advertise.filter(ads => ads.expireDate >= new Date(moment(new Date()).format("YYYY-MM-DD")).getTime()).length} &nbsp; | &nbsp;
                            <span className="schedule-available">
                                A : { 20 - getAdvertisement(advertisements, location).advertise.filter(ads => ads.expireDate >= new Date().getTime()).length }
                            </span>
                        </span>
                    </div>
                </a>
            :
            <a style={{pointerEvents: "none", cursor: "not-allowed"}}>
                <div style={{ marginLeft: '-15px', background: '#ddd', textAlign: 'center', border: '1px solid #ccc', height: '80px'}}>
                    <h4 style={{marginTop: '20px'}}>
                        <h4>{title}(<span style={{fontWeight: 'bold', fontSize: '12px'}}>{defaultSize}</span>)</h4>
                    </h4>
                    <span className="schedule-current">Not available</span>
                </div>
            </a>
    )
};
export const renderDiv = (className, classDisable, title, defaultSize, advertisements, location, handleClick) => {
    return(
        getAdvertisement(advertisements, location) != null ?
            getAdvertisement(advertisements, location).advertise.filter(ads => ads.expireDate >= new Date().getTime()).length >= 20 ?
                <a style={{pointerEvents: "none", cursor: "not-allowed"}}>
                    <div className={classDisable} style={{marginLeft: '-15px', background: '#ddd', textAlign: 'center', border: '1px solid #ccc', height: '60px'}}>
                        <h5>{title}(<span style={{fontWeight: 'bold', fontSize: '12px'}}>{defaultSize}</span>)</h5>
                        <span className="schedule-current">Not available</span>
                    </div>
                </a>
                :
                <a onClick={handleClick}>
                    <div className={className}>
                        <h5>{title}(<span style={{fontWeight: 'bold', fontSize: '12px'}}>{defaultSize}</span>)</h5>
                        <span className="schedule-current">
                            C: {getAdvertisement(advertisements, location).advertise.filter(ads => ads.expireDate >= new Date().getTime()).length} &nbsp; | &nbsp;
                            <span className="schedule-available">
                                A: {20 - getAdvertisement(advertisements, location).advertise.filter(ads => ads.expireDate >= new Date().getTime()).length}
                            </span>
                        </span>
                    </div>
                </a>
            :
            <a style={{pointerEvents: "none", cursor: "not-allowed"}}>
                <div className={classDisable}>
                    <h5>{title}(<span style={{fontWeight: 'bold', fontSize: '12px'}}>{defaultSize}</span>)</h5>
                    <span className="schedule-current">Not available</span>
                </div>
            </a>
    )
};