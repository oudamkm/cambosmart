import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { renderHeadDiv, renderDiv } from './render-form';
import './page_style_component.css';

export default class FormLocationPage extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick(location){
        if(this.props.advertisements != undefined){
            if(this.props.advertisements.length > 0){
                const advertise  = this.props.advertisements.filter(ads => ads.location == location);
                if(advertise != null){
                    this.props.handleClick({
                        location: location,
                        price: advertise[0].price,
                        description: advertise[0].description
                    });
                }
            }
        }
        window.scrollTo(0,0);
    }

    render(){
        const advertisements = this.props.advertisements;
        return(
            advertisements != undefined ?
                <div>
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("schedule-div", "LT1 ", "1122 x 300px", advertisements, "LT1", () => this.handleClick("LT1"))}
                        </Col>
                    </Row>
                    <br />

                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL1","283 x 443px ", advertisements, "LL1", () => this.handleClick("LL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL2","283 x 443px ", advertisements, "LL2", () => this.handleClick("LL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LML1","409 x 245px ",  advertisements, "LML1", () => this.handleClick("LML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LMR1","409 x 245px ", advertisements, "LMR1", () => this.handleClick("LMR1"))}
                        </Col>
                    </Row>

                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL3","283 x 443px ",  advertisements, "LL3", () => this.handleClick("LL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL4","283 x 443px ", advertisements, "LL4", () => this.handleClick("LL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LML2","409 x 245px ", advertisements, "LML2", () => this.handleClick("LML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LMR2","409 x 245px ", advertisements, "LMR2", () => this.handleClick("LMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL5","283 x 443px ", advertisements, "LL5", () => this.handleClick("LL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL6","283 x 443px ", advertisements, "LL6", () => this.handleClick("LL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LML3","409 x 245px ",  advertisements, "LML3", () => this.handleClick("LML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LMR3","409 x 245px ", advertisements, "LMR3", () => this.handleClick("LMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL7","283 x 443px ",  advertisements, "LL7", () => this.handleClick("LL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL8","283 x 443px ", advertisements, "LL8", () => this.handleClick("LL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LML4","409 x 245px ", advertisements, "LML4", () => this.handleClick("LML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("advertisement-div-cm", "advertisement-div-disable-default", "LMR4","409 x 245px ", advertisements, "LMR4", () => this.handleClick("LMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL9","283 x 443px ",  advertisements, "LL9", () => this.handleClick("LL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("advertisement-div-cl", "advertisement-div-disable-left", "LL10","283 x 443px ",  advertisements, "LL10", () => this.handleClick("LL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
