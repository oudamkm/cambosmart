import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Table, Panel } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FormSubmit from './../../../../shared_component/redux_form_fields/form_submit';
import FormDatePicker from './../../../../shared_component/redux_form_fields/form_datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './../../../../../../../node_modules/sweetalert/dist/sweetalert.css';
import {fetchViewWebsiteAction} from '../../../../../actions/admin/common';

let filter = {
    fromDate: '1000-1-1',
    toDate: '1000-1-1'
};

class VisitorView extends React.Component {
    constructor() {
        super();
        this.state = {
            startDate: null,
            endDate: null,
        };

        this.handleFromDate = this.handleFromDate.bind(this);
        this.handleToDate = this.handleToDate.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }

    componentWillMount() {
        this.props.fetchViewWebsiteAction(filter);
    }

    handleFromDate(date) {
        this.setState({
            startDate: date
        });
    }

    handleToDate(date) {
        this.setState({
            endDate: date
        });
    }

    formSubmit(value) {
        let startDate = '1000-1-1';
        let endDate = '1000-1-1';
        if (value.startDate != undefined && value.endDate != undefined) {
            startDate = value.startDate;
            endDate = value.endDate;
        }
        filter = {
            fromDate: startDate,
            toDate: endDate
        };
        this.props.fetchViewWebsiteAction(filter);
    }


    render() {
        const {handleSubmit, submitting} = this.props;
        const visitors = this.props.fetchWebsite.visitors;
        return (
            <div>
                <br/>
                <div className="row">
                    <form onSubmit={handleSubmit(this.formSubmit)}>
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-lg-3">
                                <Field name="startDate" component={FormDatePicker} placeholder="From Date" defaultDate={this.state.startDate} handleChange={this.handleFromDate}/>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-3">
                                <Field name="endDate" component={FormDatePicker} placeholder="To Date" defaultDate={this.state.endDate} handleChange={this.handleToDate}/>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-lg-2">
                                <FormSubmit submitting={submitting} label="SEARCH" icon="fa fa-search"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="row">
                    <div className="col-lg-4">
                        <Panel header="Number of Visitor's view">
                            <Table responsive>
                                <tbody>
                                <tr>
                                    <td><strong>Total Views</strong></td>
                                    <td>{ visitors === undefined ? null : visitors.length <= 0 ? 0 : visitors[0].total}</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Panel>
                    </div>
                </div>
            </div>
        );
    }
}


VisitorView = reduxForm({
    form: 'form_visitor_view',
    validate: function (values) {
        const errors = {};
        if (new Date(values.startDate).getTime() >= new Date(values.endDate).getTime()) {
            errors.endDate = 'It must greater than FROM DATE !!'
        }
        return errors
    }
})(VisitorView);

function mapStateToProps(state) {
    return{
        fetchWebsite: state.fetchWebsite
    };
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchViewWebsiteAction}, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(VisitorView)
