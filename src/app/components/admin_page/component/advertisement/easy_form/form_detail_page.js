import React from 'react';
import { Row, Col } from 'react-bootstrap';
import {renderHeadDiv, renderDiv } from './render-form';
export default class FormDetailPage extends React.Component {

    handleClick(value){
        this.props.handleClick(value);
    }

    render(){
        return(
            this.props.advertisements != undefined ?
                <div>
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("DT1 (1122 x 300px)", this.props.advertisements, "DT1", () => this.handleClick("DT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL1 (283 x 443px)", this.props.advertisements, "DL1", () => this.handleClick("DL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL2 (283 x 443px)", this.props.advertisements, "DL2", () => this.handleClick("DL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DML1 (409 x 245px)", this.props.advertisements, "DML1", () => this.handleClick("DML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DMR1 (409 x 245px)", this.props.advertisements, "DMR1", () => this.handleClick("DMR1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL3 (283 x 443px)", this.props.advertisements, "DL3", () => this.handleClick("DL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL4 (283 x 443px)", this.props.advertisements, "DL4", () => this.handleClick("DL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DML2 (409 x 245px)", this.props.advertisements, "DML2", () => this.handleClick("DML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DMR2 (409 x 245px)", this.props.advertisements, "DMR2", () => this.handleClick("DMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL5 (283 x 443px)", this.props.advertisements, "DL5", () => this.handleClick("DL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL6 (283 x 443px)", this.props.advertisements, "DL6", () => this.handleClick("DL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DML3 (409 x 245px)", this.props.advertisements, "DML3", () => this.handleClick("DML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DMR3 (409 x 245px)", this.props.advertisements, "DMR3", () => this.handleClick("DMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL7 (283 x 443px)", this.props.advertisements, "DL7", () => this.handleClick("DL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL8 (283 x 443px)", this.props.advertisements, "DL8", () => this.handleClick("DL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DML4 (409 x 245px)", this.props.advertisements, "DML4", () => this.handleClick("DML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","DMR4 (409 x 245px)", this.props.advertisements, "DMR4", () => this.handleClick("DMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL9 (283 x 443px)", this.props.advertisements, "DL9", () => this.handleClick("DL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "DL10 (283 x 443px)", this.props.advertisements, "DL10", () => this.handleClick("DL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
