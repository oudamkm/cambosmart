import React from 'react';
import { Row, Col } from 'react-bootstrap';
import {renderHeadDiv, renderDiv } from './render-form';

export default class FormCategoryPage extends React.Component {

    handleClick(value){
        this.props.handleClick(value);
    }

    render(){
        return(
            this.props.advertisements != undefined ?
                <div>
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("CT1 (1122 x 300px)", this.props.advertisements, "CT1", () => this.handleClick("CT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lgOffset={4} lg={8}>
                            {renderHeadDiv("CT2 (838 x 298px)", this.props.advertisements, "CT2", () => this.handleClick("CT2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL1 (283 x 443px)", this.props.advertisements, "CL1", () => this.handleClick("CL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL2 (283 x 443px)", this.props.advertisements, "CL2", () => this.handleClick("CL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CML1 (409 x 245px)", this.props.advertisements, "CML1", () => this.handleClick("CML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CMR1 (409 x 245px)", this.props.advertisements, "CMR1", () => this.handleClick("CMR1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL3 (283 x 443px)", this.props.advertisements, "CL3", () => this.handleClick("CL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL4 (283 x 443px)", this.props.advertisements, "CL4", () => this.handleClick("CL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CML2 (409 x 245px)", this.props.advertisements, "CML2", () => this.handleClick("CML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CMR2 (409 x 245px)", this.props.advertisements, "CMR2", () => this.handleClick("CMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL5 (283 x 443px)", this.props.advertisements, "CL5", () => this.handleClick("CL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL6 (283 x 443px)", this.props.advertisements, "CL6", () => this.handleClick("CL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CML3 (409 x 245px)", this.props.advertisements, "CML3", () => this.handleClick("CML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CMR3 (409 x 245px)", this.props.advertisements, "CMR3", () => this.handleClick("CMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL7 (283 x 443px)", this.props.advertisements, "CL7", () => this.handleClick("CL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL8 (283 x 443px)", this.props.advertisements, "CL8", () => this.handleClick("CL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CML4 (409 x 245px)", this.props.advertisements, "CML4", () => this.handleClick("CML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","CMR4 (409 x 245px)", this.props.advertisements, "CMR4", () => this.handleClick("CMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL9 (283 x 443px)", this.props.advertisements, "CL9", () => this.handleClick("CL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "CL10 (283 x 443px)", this.props.advertisements, "CL10", () => this.handleClick("CL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
