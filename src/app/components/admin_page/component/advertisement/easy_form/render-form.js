import React from 'react';

export const renderHeadDiv = (title, advertisements, location, handleClick) => {
    if(advertisements.find(advertisement => advertisement.location == location) != null){
        return(
            <a style={{pointerEvents: "none", cursor: "not-allowed"}}>
                <div
                    style={{background: '#ddd', textAlign: 'center', border: '1px solid #ccc', height: '60px'}}>
                    <h4 style={{marginTop: '20px'}}>
                        {title}
                    </h4>
                </div>
            </a>
        )
    }else {
        return(
            <a onClick={handleClick}>
                <div style={{ textAlign: 'center', border: '1px solid #ccc', height: '60px'}}>
                    <h4 style={{marginTop: '20px'}}>
                        {title}
                    </h4>
                </div>
            </a>
        )
    }
};

export const renderDiv = (dse, dsd, title, advertisements, location, handleClick) => {
    if(advertisements.find(advertisement => advertisement.location == location) != null){
        return(
            <a className="easy-form-div-a">
                <div className={dsd}>
                    <h5 style={{marginTop: "10px"}}>
                        {title}
                    </h5>
                </div>
            </a>
        )
    }else {
        return(
            <a onClick={handleClick}>
                <div className={dse}>
                    <h5 style={{marginTop: "10px"}}>
                        {title}
                    </h5>
                </div>
            </a>
        )
    }
};
