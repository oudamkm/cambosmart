import React from 'react';
import { Row, Col } from 'react-bootstrap';
import {renderHeadDiv, renderDiv } from './render-form';

export default class FormLocationPage extends React.Component {

    handleClick(value){
        this.props.handleClick(value);
    }

    render(){
        return(
            this.props.advertisements != undefined ?
                <div>
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("LT1 (1122 x 300px)", this.props.advertisements, "LT1", () => this.handleClick("LT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL1 (283 x 443px)", this.props.advertisements, "LL1", () => this.handleClick("LL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL2 (283 x 443px)", this.props.advertisements, "LL2", () => this.handleClick("LL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LML1 (409 x 245px)", this.props.advertisements, "LML1", () => this.handleClick("LML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LMR1 (409 x 245px)", this.props.advertisements, "LMR1", () => this.handleClick("LMR1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL3 (283 x 443px)", this.props.advertisements, "LL3", () => this.handleClick("LL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL4 (283 x 443px)", this.props.advertisements, "LL4", () => this.handleClick("LL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LML2 (409 x 245px)", this.props.advertisements, "LML2", () => this.handleClick("LML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LMR2 (409 x 245px)", this.props.advertisements, "LMR2", () => this.handleClick("LMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL5 (283 x 443px)", this.props.advertisements, "LL5", () => this.handleClick("LL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL6 (283 x 443px)", this.props.advertisements, "LL6", () => this.handleClick("LL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LML3 (409 x 245px)", this.props.advertisements, "LML3", () => this.handleClick("LML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LMR3 (409 x 245px)", this.props.advertisements, "LMR3", () => this.handleClick("LMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL7 (283 x 443px)", this.props.advertisements, "LL7", () => this.handleClick("LL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL8 (283 x 443px)", this.props.advertisements, "LL8", () => this.handleClick("LL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LML4 (409 x 245px)", this.props.advertisements, "LML4", () => this.handleClick("LML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("easy-form-div-dm-enable", "easy-form-div-dm-disable","LMR4 (409 x 245px)", this.props.advertisements, "LMR4", () => this.handleClick("LMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL9 (283 x 443px)", this.props.advertisements, "LL9", () => this.handleClick("LL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("easy-form-div-dl-enable","easy-form-div-dl-disable", "LL10 (283 x 443px)", this.props.advertisements, "LL10", () => this.handleClick("LL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
