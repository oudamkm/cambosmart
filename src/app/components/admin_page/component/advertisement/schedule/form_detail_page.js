import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { renderHeadDiv, renderDiv } from './render-form';

export default class FormDetailPage extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick(location){
        window.scrollTo(0,0);
        this.props.handleClick({list: true, location: location});
    }

    render(){
        const advertisements = this.props.advertisements;
        return(
            advertisements != undefined ?
                <div className="col-lg-4">
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("schedule-div", "DT1", advertisements, "DT1", () => this.handleClick("DT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL1", advertisements, "DL1", () => this.handleClick("DL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL2", advertisements, "DL2", () => this.handleClick("DL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "DML1",  advertisements, "DML1", () => this.handleClick("DML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "DMR1", advertisements, "DMR1", () => this.handleClick("DMR1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL3", advertisements, "DL3", () => this.handleClick("DL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL4", advertisements, "DL4", () => this.handleClick("DL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "DML2", advertisements, "DML2", () => this.handleClick("DML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "DMR2", advertisements, "DMR2", () => this.handleClick("DMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL5", advertisements, "DL5", () => this.handleClick("DL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL6", advertisements, "DL4", () => this.handleClick("DL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "DML3", advertisements, "DML3", () => this.handleClick("DML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "DMR3", advertisements, "DMR3", () => this.handleClick("DMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL7", advertisements, "DL7", () => this.handleClick("DL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL8", advertisements, "DL8", () => this.handleClick("DL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "DML4", advertisements, "DML4", () => this.handleClick("DML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "DMR4", advertisements, "DMR4", () => this.handleClick("DMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL9", advertisements, "DL9", () => this.handleClick("DL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "DL10", advertisements, "DL10", () => this.handleClick("DL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
