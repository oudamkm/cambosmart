import React from 'react';

const getAdvertisement = (advertisements, location) => {
    return advertisements.find(advertisement => advertisement.location == location)
};

export const renderHeadDiv = (className, title, advertisements, location, handleClick) => {
    return(
        <a onClick={handleClick}>
            <div className={className}>
                <h4>{title}</h4>
                {getAdvertisement(advertisements, location) != null ?
                    <span className="schedule-current">
                        C : {getAdvertisement(advertisements, location).advertise.length} &nbsp; => &nbsp;
                        <span className="schedule-available">
                            A : {20 - getAdvertisement(advertisements, location).advertise.length}
                        </span>
                    </span>
                    :
                    <span className="schedule-current">C : 0 &nbsp; => &nbsp;
                        <span className="schedule-available">A : 20</span>
                    </span>
                }
            </div>
        </a>
    )
};

export const renderDiv = (className, title, advertisements, location, handleClick) => {
    return(
        <a onClick={handleClick}>
            <div className={className}>
                <h5>{title}</h5>
                {getAdvertisement(advertisements, location) != null ?
                    <span className="schedule-current">
                        C : {getAdvertisement(advertisements, location).advertise.length} &nbsp; => &nbsp;
                        <span className="schedule-available">
                            A : {20 - getAdvertisement(advertisements, location).advertise.length}
                        </span>
                    </span>
                    :
                    <span className="schedule-current">C : 0 &nbsp; => &nbsp;
                        <span className="schedule-available">
                            A : 20
                        </span>
                    </span>
                }
            </div>
        </a>
    )
};