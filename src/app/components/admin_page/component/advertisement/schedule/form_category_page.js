import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { renderHeadDiv, renderDiv } from './render-form';

export default class FormCategoryPage extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick(location){
        window.scrollTo(0,0);
        this.props.handleClick({list: true, location: location});
    }

    render(){
        const advertisements = this.props.advertisements;
        return(
            advertisements != undefined ?
                <div className="col-lg-4">
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("schedule-div", "CT1", advertisements, "CT1", () => this.handleClick("CT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lgOffset={4} lg={8} style={{marginLeft: '115px'}}>
                            {renderHeadDiv("schedule-div", "CT2", advertisements, "CT2", () => this.handleClick("CT2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL1", advertisements, "CL1", () => this.handleClick("CL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL2", advertisements, "CL2", () => this.handleClick("CL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "CML1",  advertisements, "CML1", () => this.handleClick("CML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "CMR1", advertisements, "CMR1", () => this.handleClick("CMR1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL3", advertisements, "CL3", () => this.handleClick("CL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL4", advertisements, "CL4", () => this.handleClick("CL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "CML2", advertisements, "CML2", () => this.handleClick("CML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "CMR2", advertisements, "CMR2", () => this.handleClick("CMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL5", advertisements, "CL5", () => this.handleClick("CL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL6", advertisements, "CL4", () => this.handleClick("CL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "CML3", advertisements, "CML3", () => this.handleClick("CML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "CMR3", advertisements, "CMR3", () => this.handleClick("CMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL7", advertisements, "CL7", () => this.handleClick("CL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL8", advertisements, "CL8", () => this.handleClick("CL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "CML4", advertisements, "CML4", () => this.handleClick("CML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "CMR4", advertisements, "CMR4", () => this.handleClick("CMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL9", advertisements, "CL9", () => this.handleClick("CL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "CL10", advertisements, "CL10", () => this.handleClick("CL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
