import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { renderHeadDiv, renderDiv } from './render-form';

export default class FormLocationPage extends React.Component {

    constructor(props){
        super(props);
    }

    handleClick(location){
        window.scrollTo(0,0);
        this.props.handleClick({list: true, location: location});
    }

    render(){
        const advertisements = this.props.advertisements;
        return(
            advertisements != undefined ?
                <div className="col-lg-4">
                    <Row>
                        <Col lg={12}>
                            {renderHeadDiv("schedule-div", "LT1", advertisements, "LT1", () => this.handleClick("LT1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL1", advertisements, "LL1", () => this.handleClick("LL1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL2", advertisements, "LL2", () => this.handleClick("LL2"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "LML1",  advertisements, "LML1", () => this.handleClick("LML1"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "LMR1", advertisements, "LMR1", () => this.handleClick("LMR1"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL3", advertisements, "LL3", () => this.handleClick("LL3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL4", advertisements, "LL4", () => this.handleClick("LL4"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "LML2", advertisements, "LML2", () => this.handleClick("LML2"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "LMR2", advertisements, "LMR2", () => this.handleClick("LMR2"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL5", advertisements, "LL5", () => this.handleClick("LL5"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL6", advertisements, "LL4", () => this.handleClick("LL6"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "LML3", advertisements, "LML3", () => this.handleClick("LML3"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "LMR3", advertisements, "LMR3", () => this.handleClick("LMR3"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL7", advertisements, "LL7", () => this.handleClick("LL7"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL8", advertisements, "LL8", () => this.handleClick("LL8"))}
                        </Col>
                        <Col lg={4}>
                            {renderDiv("schedule-div-r", "LML4", advertisements, "LML4", () => this.handleClick("LML4"))}
                        </Col>
                        <Col lg={5}>
                            {renderDiv("schedule-div-r", "LMR4", advertisements, "LMR4", () => this.handleClick("LMR4"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL9", advertisements, "LL9", () => this.handleClick("LL9"))}
                        </Col>
                    </Row>
                    <br />
                    <Row>
                        <Col lg={3}>
                            {renderDiv("schedule-div-l", "LL10", advertisements, "LL10", () => this.handleClick("LL10"))}
                        </Col>
                    </Row>
                </div>
                :
                null
        )
    }
}
