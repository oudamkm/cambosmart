import React, { Component } from 'react';
import axios from 'axios';
import FormData from '../../../../node_modules/form-data';
import Cropper from '../../../../node_modules/react-crop';
import '../../../../node_modules/react-crop/cropper.css';
// You'll need to use async functions
import "babel-core/register";
import "babel-polyfill";
import Modal from '../../../../node_modules/react-bootstrap/lib/Modal';
import Button from '../../../../node_modules/react-bootstrap/lib/Button';
import { saveState, loadState, clearState } from './../../localstorages/local_storage';
import { API_ENDPOINT, AUTH_CONFIG } from './../../api/headers';
let data = loadState();

export default class ProfileCrop extends Component {
    constructor() {
        super();
        this.state = {
            image: null,
            imageName: "",
            previewImage: `/images/profiles/${data.user.profileImage}`
        };

        this.onChange = this.onChange.bind(this);
        this.handleUploadFile = this.handleUploadFile.bind(this);
        this.crop = this.crop.bind(this);
    }

    componentWillMount(){
        this.setState({ previewImage: `/images/profiles/${data.user.profileImage}`})
    }

    onChange(e) {
        this.setState({image: e.target.files[0], imageName: e.target.files[0].name});
    }

    handleUploadFile(file){
        let formData = new FormData();
        formData.append('file', file, this.state.imageName);
        axios.post(API_ENDPOINT+'users/member/'+data.user.userId+'/upload-image', formData, AUTH_CONFIG(data.token))
            .then(function (res) {
                data.user.profileImage = res.data.data.profileImage;
                saveState(data);
                location.href = `/${data.user.userType}/profile`;
            })
            .catch(function (error) {
                if (error.response.status) {
                    if (error.response.status === 401) {
                        clearState();
                    } else if (error.response.status === 500) {
                        window.location.assign('/server/error')
                    } else {
                        window.location.assign('/server/down')
                    }
                } else {
                    window.location.assign('/server/down')
                }
            });
        this.refs.file.value = null;
        this.setState({ image: null})
    }

    async crop() {
        const image = await this.refs.crop.cropImage();
        this.handleUploadFile(image);
    }

    clear() {
        this.refs.file.value = null;
        this.setState({ image: null})
    }

    imageLoaded(img) {
        if (img.naturalWidth && img.naturalWidth < 262 &&
            img.naturalHeight && img.naturalHeight < 147) {
            this.crop()
        }
       alert(img.width + ' : ' + img.naturalWidth);
    }


    render() {
        return (
            <div style={{marginLeft: '-25px'}}>
                <form encType="multipart/form-data">
                    <div className="avatar-photo">
                        <input ref='file' type='file' accept="image/*" onChange={this.onChange.bind(this)} />
                        <div className="avatar-edit">
                            <span>CLICK HERE</span>
                        </div>
                        <img src={this.state.previewImage} />
                    </div>
                    { this.state.image &&
                    <div className="static-modal">
                        <Modal.Dialog>
                            <Modal.Header>
                                <Modal.Title >Create profile picture</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div style={{position: 'relative'}}>
                                    <Cropper
                                        ref='crop'
                                        image={this.state.image}
                                        heightLabel={400}
                                        width={168}
                                        height={168}
                                        onImageLoaded={this.imageLoaded.bind(this)}
                                        minConstraints={[400,225]}
                                    />
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={this.clear.bind(this)}>CLOSE</Button>
                                <Button onClick={this.crop.bind(this)}>SAVE</Button>
                            </Modal.Footer>
                        </Modal.Dialog>
                    </div>
                    }
                </form>
            </div>
        );
    }
}