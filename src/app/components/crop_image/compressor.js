/**
 * Function compressor use to compress image file before uploading
 * @param imgUrl
 * @param quality (> 0 && <= 1)
 * @returns {*|string}
 */
export const profileCompress = (imgUrl, quality) => {
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    let img = new Image();
    img.src = imgUrl;
    canvas.width = img.width;
    canvas.height = img.height;
    ctx.drawImage(img, 0, 0, img.width, img.height);
    return canvas.toDataURL('image/jpeg', quality);
};

/**-------------------------------Using Jquery with Canvas HTML5-------------------------------------------------*/

let $ = require("jquery");
export const compressImage = (base64, width, height) => {
    var canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = height;
    var context = canvas.getContext("2d");
    var deferred = $.Deferred();
    $("<img/>").attr("src", base64).on('load', function() {
        context.scale(width/50,  height/50);
        context.drawImage(this, 0, 0);
        deferred.resolve($("<img/>").attr("src", canvas.toDataURL()));
    });
    return deferred.promise();
};