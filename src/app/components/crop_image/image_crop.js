import React from 'react';
import axios from 'axios';
import FormData from '../../../../node_modules/form-data';
import { connect } from 'react-redux';
import AvatarCropper from "react-avatar-cropper";
import FileUpload from './file_upload';
import './style.css';
import './prism.css';
import { saveState, loadState } from './../../localstorages/local_storage';
import { API_ENDPOINT, AUTH_CONFIG } from './../../api/headers';
import {dataURItoBlob} from './../../utils/image_file_handling'
let data = loadState();

class ImageCrop extends React.Component {
    constructor() {
        super();
        this.state = {
            cropperOpen: false,
            img: null,
            size: 0,
            name: '',
            croppedImg: `/images/profiles/${data.user.profileImage}`
        };
        this.handleCrop = this.handleCrop.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleRequestHide = this.handleRequestHide.bind(this);
        this.handleUploadFile = this.handleUploadFile.bind(this);
    }

    componentWillMount(){
        this.setState({croppedImg: `/images/profiles/${data.user.profileImage}`})
    }

    handleUploadFile(file, name){
        let formData = new FormData();
        formData.append('file', file, name);
        axios.post(API_ENDPOINT+'users/member/'+data.user.userId+'/upload-image', formData, AUTH_CONFIG(data.token))
            .then(function (res) {
                data.user.profileImage = res.data.data.profileImage;
                saveState(data);
            })
            .catch(function (err) {
                if(err.response.status === 200){
                    location.href = `/${data.user.userType}/profile`
                }else if(err.response.status === 401){
                    location.href = "/sign-in";
                }else {}
            });
    }

    handleFileChange (data) {
        console.log(data.size);
        if(data.size === 1){
            this.props.imageError("File was too large, please try another !!");
        }else if(data.size === 0){
            this.props.imageError("File was too small, please try another !!");
        }else if(data.size === -1){
            this.props.imageError("File did not match any type of (jpeg, jpg, png) !!");
        }else {
            this.props.imageError("");
            this.setState({
                img: data.img,
                size: data.size,
                name: data.name,
                croppedImg: this.state.croppedImg,
                cropperOpen: true
            });
        }
    }

    /**======cropping image============*/
    handleCrop (dataURI) {
        let img = document.createElement("img");
        let imageUri = "";
        img.src = dataURI;
        img.onload = function () {
            let canvas = document.createElement('canvas');
            let ctx = canvas.getContext('2d');
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0, img.width, img.height);
            imageUri = canvas.toDataURL('image/jpeg', 0.8);
        };
        setTimeout(() => {
            this.handleUploadFile(dataURItoBlob(imageUri), this.state.name.toLowerCase().concat(".jpg"));
            this.setState({
                cropperOpen: false,
                img: null,
                size: 0,
                name: '',
                croppedImg: dataURI
            });
        }, 500)
    }

    handleRequestHide () {
        this.setState({cropperOpen: false});
    }

    render() {
        return (
            <div className="row">
                <form encType="multipart/form-data">
                    <div className="avatar-photo">
                        <FileUpload handleFileChange={this.handleFileChange} />
                        <div className="avatar-edit">
                            <span>CLICK HERE</span>
                        </div>
                        <img src={this.state.croppedImg} />
                    </div>
                    {this.state.cropperOpen &&
                    <AvatarCropper
                        onRequestHide={this.handleRequestHide}
                        cropperOpen={this.state.cropperOpen}
                        onCrop={this.handleCrop}
                        image={this.state.img}
                        width={200}
                        height={200}
                        closeButtonCopy="CLOSE"
                        cropButtonCopy="SAVE"
                    />
                    }
                </form>
            </div>
        );
    }
}

export default connect()(ImageCrop)
