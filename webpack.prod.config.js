const webpack = require("webpack");
let CompressionPlugin = require("compression-webpack-plugin");
module.exports = require('./webpack.config.js');    // inherit from the main config file
// disable the hot reload
module.exports.devtool = 'source-map';
module.exports.entry = [
    'babel-polyfill',
    module.exports.SRC_DIR + '/index.js'
];
// production env
module.exports.plugins.push(
    new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production')
        }
    })
);
// compress the js file
module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
        sourceMap: false,
        compress: {
            warnings: false,
            sequences: true,
            dead_code: true,
            conditionals: true,
            booleans: true,
            unused: true,
            if_return: true,
            join_vars: true,
            drop_console: true
        },
        mangle: {
            except: ['$super', '$', 'exports', 'require']
        },
        minimize: true,
        output: {
            comments: false
        }
    })
);
module.exports.plugins.push(
    new webpack.NoErrorsPlugin()
);

module.exports.plugins.push(
    new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/])
);

module.exports.plugins.push(
    new webpack.optimize.OccurenceOrderPlugin()
);

module.exports.plugins.push(
    new webpack.optimize.AggressiveMergingPlugin()
);

module.exports.plugins.push(
    new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.(js|html)$/,
        threshold: 10240,
        minRatio: 0.8
    })
);