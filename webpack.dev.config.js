const webpack = require("webpack");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
module.exports = require('./webpack.config.js');    // inherit from the main config file

module.exports.devtool = 'eval-source-map';
// disable the hot reload
module.exports.entry = [
    'babel-polyfill',
    module.exports.SRC_DIR + '/index.js'
];
// export css to a separate file
module.exports.module.loaders[1] = {
    test: /\.scss$/,
    loader: ExtractTextPlugin.extract('css!sass')
};
module.exports.plugins.push(
    new ExtractTextPlugin('../css/main.css')
);

module.exports.devServer = {
    contentBase: module.exports.PUBLIC_DIR,
        disableHostCheck:true,
        host:'0.0.0.0',
        port: 8080
};